Project: LEDs Animation. 
Author: Jeronimo Jaramillo B
Date: 7/05/22

This project has the main purpose of the basic handling of GPIOs of the STM32F429IZ-Discovery board.
6 LEDs have been connected to the STM32 board, where the sequence of LEDs starts when the user button is pressed.

If the button is pushed for the first time, the MCU executes the first sequence of the Led's light at a low speed.
If the button is pushed for the second time, the sequence of the LED's light increases the medium speed of the transitions. 
If the button is pushed for the third time, the sequence of the LED's light increases the high speed of the transitions. 
